import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterestingPage } from './interesting.page';

describe('InterestingPage', () => {
  let component: InterestingPage;
  let fixture: ComponentFixture<InterestingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterestingPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterestingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
