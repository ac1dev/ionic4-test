import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePage } from './home/home.page';
import { InterestingPage } from './interesting/interesting.page';
import { ModlPage } from './modl/modl.page';
import { SevenPage } from './seven/seven.page';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomePage },
  { path: 'one', component: InterestingPage },
  { path: 'two', component: ModlPage },
  { path: 'three', component: SevenPage },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
