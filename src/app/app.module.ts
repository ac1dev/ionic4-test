import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SevenPage } from './seven/seven.page';
import { InterestingPage } from './interesting/interesting.page';
import { ModlPage } from './modl/modl.page';
import { HomePage } from './home/home.page';

@NgModule({
  declarations: [AppComponent, SevenPage, InterestingPage, ModlPage, HomePage],
  entryComponents: [SevenPage, InterestingPage, ModlPage, HomePage],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
