import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModlPage } from '../modl/modl.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(public modalController: ModalController) {

  }

  async openModal() {
    const modal = await this.modalController.create({ component: ModlPage });
    modal.present();
  }

}
