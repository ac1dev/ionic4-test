import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModlPage } from './modl.page';

describe('ModlPage', () => {
  let component: ModlPage;
  let fixture: ComponentFixture<ModlPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModlPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModlPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
